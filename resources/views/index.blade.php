<?php

//Colocando váriavel dentro de outra variável - Aqui temos um código HTML dentro de uma variável PHP.

use PhpParser\Node\Stmt\Echo_;

$nome = "João Paulo";

$html = "<!DOCTYPE html>
 <head>
     <title>Site em PHP de $nome</title>
 </head>
 <body>
     <h1>Meu nome é $nome</h1>
     <p>Olá $nome</p>
 </body>
</html>";

#Podemos criar comentários com duas barras, com # ou com /* AQUI VAI VIRAR UM COMENTÁRIO */
/* Testando o comentário para grandes textos. */

echo $html;

#Declarando variável.
$a = 100;
$b = '100';
$c = 3.14;
$d = TRUE;
$d2 = FALSE;

//Imprimindo na tela.
echo $a;
echo '<br>';  //Quebra de linha.
echo $b;
echo '<br>';
echo $c;
echo '<br>';
echo $d;
echo '<br>';
echo $d2;
echo '<br>';
//Declaração de variável.
$num_1 = 2;
$num_2 = 5;

//Operadores aritiméticos.
$soma = $num_1 + $num_2;  //Soma.
$subtracao = $num_1 - $num_2;  //Subtração.
$mult = $num_1 * $num_2;  //Multiplicação.
$div = $num_1 / $num_2;  //Divisão.

//Imprimindo resultados das operações anteriores na tela.
echo "O resultado da soma entre numero $num_1 e o numero $num_2 é $soma", '<br>';
echo "O resultado da subtração entre numero $num_1  e o numero $num_2 é $subtracao", '<br>';
echo "O resultado da multiplicação entre o numero $num_1  e o numero $num_2 é $mult", '<br>';
echo "O resultado da divisão entre o numero $num_1 e o  numero $num_2 é $div", '<br>';
echo '<br>';

//Arrays.
$prateleira = ['O', 'l', 'á', ',', ' ', 'Mundo', '!', 1, 2, 3, 4, 5];

echo $prateleira[0];
echo $prateleira[1];
echo $prateleira[2];
echo $prateleira[3];
echo $prateleira[4];
echo $prateleira[5];
echo $prateleira[6];
echo '<br>';
echo '<br>';
#Nesse caso acima a variável $prateleira armazena várias informações, para imprimir temos que associar qual informação deve ser impressa começando do 0 para o primeiro campo. - Na tela foi impresso Olá, Mundo!

//Array associativo
$compras = ['a' => 'abacate', 'b' => 'bola', 'c' => 'camarão']; //Nesse caso o abacate é acessavel pela letra "a" o "a" é uma chave.

echo $compras['a'], '<br>';
echo $compras['b'], '<br>';
echo $compras['c'], '<br>';
echo '<br>';

echo "Eu comprei um " . $compras['a'] . " uma " . $compras['b'] . " e um pacote de " . $compras['c'];
echo '<br>';
echo '<br>';

//Imprimir todo conteúdo do Array.
print_r($prateleira);
echo '<br>';
echo '<br>';

var_dump($prateleira);
echo '<br>';
echo '<br>';
var_dump($compras);
echo '<br>';
echo '<br>';


//Estruturas condicionais.
$a = 100;
$b = 50;

if ($a === $b) {   //Operador de comparação Idêntico.
    echo 'Valores iguais e de tipos iguais.';
} elseif ($a == $b) {   //Operador de comparação Igual.
    echo 'Valores iguais mais de tipos diferentes.';
} else {
    echo 'Valores diferentes.';
}

echo '<br>';
echo '<br>';

if ($a != $b) {   //Operador de comparação Diferente (também pode ser usado <>).
    echo 'Valores diferentes.';
} else {
    echo 'Valores iguais';
}

echo '<br>';
echo '<br>';

if ($a !== $b) {   //Operador de comparação Não Idêntico.
    echo 'Valores não idênticos';
} else {
    echo 'Valores idênticos';
}

echo '<br>';
echo '<br>';

if ($a < $b) {   //Operador de comparação Menor que.
    echo "O valor $a é menor que o valor $b.";
} elseif ($a > $b) {   //Operador de comparação Maior que.
    echo "O valor $a é maior que o valor $b.";
} else {
    echo "Os valores $a e $b são iguais.";
}

echo '<br>';
echo '<br>';

if ($a <= $b) {   //Operador de comparação Menor ou igual a.
    echo "O valor $a é menor ou igual a $b.";
} else {
    echo "O valor $a e maior que $b.";
}

echo '<br>';
echo '<br>';

if ($a >= $b) {   //Operador de comparação Maior ou igual a.
    echo "O valor $a é maior ou igual a $b.";
} else {
    echo "O valor $a é menor que $b.";
}


echo '<br>';
echo '<br>';


echo '<br>';
echo '<br>';


//Módulo % - da o resto da divisão de dois numeros.
$d = 51;
$e = 2;
if ($d % $e == 0) {
    echo "O numero $d é Par.";
} else {
    echo "O numero $d é Impar!";
}

echo '<br>';
echo '<br>';


//Operadores Lógicos.
$a = 100;
$b = 90;
$c = 10;
if (($a > $b) and ($b > $c)) {   //Executa se as duas operações forem verdadeiras (também pode usar &&).
    echo "$a é maior que $b e $b é maior que $c.";
} else {
    echo "False";
}

echo '<br>';
echo '<br>';

if (($a > $b) or ($b > $c)) {   //Executa se uma duas operações forem verdadeiras (também pode usar ||).
    echo "True";
} else {
    echo "False";
}

echo '<br>';
echo '<br>';

if (($a > $b) xor ($b > $c)) {   //Executa se uma duas operações forem verdadeiras mais não ambas (também pode usar ||).
    echo "True";
} else {
    echo "False";
}

echo '<br>';
echo '<br>';

if (!($a > $b)) {   //Verdadeiro se $a não for verdadeiro.
    echo "True";
} else {
    echo "False";
}


echo '<br>';
echo '<br>';

//Extruturas de controle.

//Forma se sintaxe alternativa de fazer condições if e else.
$a = 6;
$b = 2;
if ($a % $b == 1) : //Extrurura de controle if e else.
    echo 'Numero Impar.';
else :
    echo 'Numero par.';
endif;
?> <!-- Fechei a tag php que abri no inicio. -->

<?php echo '<br>'; ?>
<?php echo '<br>'; ?>

<?php if ($a % $b == 1) : ?>
    <p>Numero Impar.</p>

<?php else : ?>
    <p>Numero par.</p>

<?php endif; ?>

<?php

//Operador ternário
$n = 5;

$tipo = $n % 2 == 0 ? 'Ternario Numero par' : 'Ternario Numero impar';

echo $tipo;


echo '<br>';
echo '<br>';

//Extrutura de controle while e dowhile
$num = 0;
while ($num < 5) { //While só vai executar o código se resultado da operação for verdadeira.
    echo "O numero $num é menor que 5 <br>";  //Utilize a tab <br> para quebra de linha dentro da string
    $num = $num + 1;
}

$c = 0;

echo '<br>';
echo '<br>';

while ($c < 5) {   //Esse é o mesmo cógido anterior mais utilizando ++.
    echo "O numero $c é menor que 5 <br>";;
    $c++;   //A variável $c vai receber ele +1 conforme o laço de repetição.
}


echo '<br>';
echo '<br>';


$d = 0;
do {   //O Dowhile vai executar o laço uma vez e testar pra saber ser é verdadeiro, ser for volta o laço.
    echo $d, ' ';
    $d++;
} while ($d <= 10);


echo '<br>';
echo '<br>';


//Extrutura de repetição For e Foreach

//Comando for.
for ($i = 0; $i < 5; $i++) {
    echo 'O valor de $i é ' . $i . '<br>';
}


echo '<br>';
echo '<br>';


$compras = ['Arroz', 'Açucar', 'Tomate', 15.6, 100];   //Usando o For para imprimir os itens de uma Array.

for ($i = 0; $i < 5; $i++) {
    echo $compras[$i] . '<br>';
}


echo '<br>';
echo '<br>';


$compras = ['Arroz', 'Açucar', 'Tomate', 15.6, 100, 'Abacaxi', 15.5];   //Usando o For para imprimir os itens de uma Array.

for ($i = 0; $i < count($compras); $i++) {   //Usado o count ele conta quantos intens tenho na Array automaticamente.
    echo $compras[$i] . '<br>';
}


echo '<br>';
echo '<br>';


$compras = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];   //Usando o For para imprimir os itens de uma Array.

for ($i = count($compras) - 1; $i >= 0; $i--) {   //Comando For decremental (de trás para frente).
    echo $compras[$i] . '<br>';
}


echo '<br>';
echo '<br>';


//Comando foreach.
foreach ($compras as $key) {
    echo $key, '#';
}


echo '<br>';
echo '<br>';

?> <!-- Fechei o php para trabalhar com html. -->


<!-- Integrando html com php comando FOREACH para extrair itens de um Array. -->
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Lista de compras</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <?php $lista = ['Pão', 'Maça', 'Abacate', 'Pera', 'Feijão', 'Açucar', 'Leite condensado'] ?>
    <h1>Lista de compras</h1>
    <ul>
        <?php foreach ($lista as $item) { ?>
            <li><?= $item ?></li>
        <?php } ?>
    </ul>
</body>

</html>

<?php

//Comando break.
$lista = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

foreach ($lista as $item) {

    echo $item, '<br>';

    if ($item == 5) {
        break;   //Usei o Break para terminar o Foreach conforme solicitado.
    }
}

echo 'Encerrrado';

echo '<br>';
echo '<br>';

//Comando continue.
$lista = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
foreach ($lista as $item) {

    if ($item % 2 == 0) {
        continue;   //Usei o Break para terminar o Foreach conforme solicitado.
    }
    echo $item, '*';
}

echo 'Esses são os numeros impares';


echo '<br>';
echo '<br>';


//Comando switch
$estados = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR'];

foreach ($estados as $estado) {
    switch ($estado) {
        case 'AC':
            echo 'Acre';
            break;
        case 'AL':
            echo 'Alagoas';
            break;
        case 'AP':
            echo 'Amapá';
            break;
        case 'AM':
            echo 'Amazonas';
            break;
        case 'BA':
            echo 'Bahia';
            break;
        case 'CE':
            echo 'Ceará';
            break;
        case 'DF':
            echo 'Distrito Federal';
            break;
        case 'ES':
            echo 'Espírito Santo';
            break;
        case 'GO':
            echo 'Goias';
            break;
        case 'MA':
            echo 'Maranhão';
            break;
        case 'MT':
            echo 'Mato Grosso';
            break;
        case 'MS':
            echo 'Mato Grosso do Sul';
            break;
        case 'MG':
            echo 'Minas Gerais';
            break;
        case 'PA':
            echo 'Pará';
            break;
        case 'PB':
            echo 'Pernanbuco';
            break;
        case 'PR':
            echo 'Paraná';
            break;
    }

    echo '<br>';
}


echo '<br>';
echo '<br>';


//Comando match.
$x = 3;

$nome = match ($x) {
    1 => 'Um',
    2 => 'Dois',
    3 => 'Três',
    4 => 'Quatro',
    5 => 'cinco'
};

echo $nome;


echo '<br>';
echo '<br>';


//Include e Require.

//include
include resource_path('views/a.php');
include resource_path('views/b.php');
include resource_path('views/c.php');
include resource_path('views/d.php');
@include resource_path('views/e.php');   //A página e.php não existe e colocando o @ no include ele ignora ela, sem @ da erro.

echo '<br>';
echo '<br>';

//require
require resource_path('views/a.php');
require resource_path('views/b.php');
require resource_path('views/c.php');
require resource_path('views/d.php');
/* @require resource_path('views/e.php'); */   //A página e.php não existe e mesmo colocando o @ no include ele da erro, só funciona se a página existir. 


echo '<br>';
echo '<br>';


//Include e Require.

//include_once.
include_once resource_path('views/a.php');   //Include_once ele impede que o mesmo arquivo se repita na página.
include_once resource_path('views/b.php');
include_once resource_path('views/c.php');
include_once resource_path('views/d.php');
@include_once resource_path('views/e.php');   //A página e.php não existe e colocando o @ no include ele ignora ela, sem @ da erro.

include_once resource_path('views/b.php');
include_once resource_path('views/c.php');
include_once resource_path('views/d.php');

echo '<br>';
echo '<br>';

//require_once.
require_once resource_path('views/a.php');   //Require_once ele impede que o mesmo arquivo se repita na página.
require_once resource_path('views/b.php');
require_once resource_path('views/c.php');
require_once resource_path('views/d.php');

require_once resource_path('views/a.php');
require_once resource_path('views/b.php');
/* @require resource_path('views/e.php'); */   //A página e.php não existe e mesmo colocando o @ no include ele da erro, só funciona se a página existir.

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Curso PHP</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>

    <?php include_once resource_path('views/header.php'); ?>

    <?php include_once resource_path('views/menu.php'); ?>

    <?php include_once resource_path('views/conteudo.php'); ?>

    <?php include_once resource_path('views/rodape.php'); ?>


</body>

</html>

<?php


echo '<br>';
echo '<br>';


//Funções
function soma($n1, $n2)
{
    $soma = $n1 + $n2;

    return $soma;
}

echo soma(3, 20);   //Posso imprimir chamando a função

echo '<br>';

$result = soma(4, 6);   //Ou posso criar uma variável para receber o valor da operação e depois imprimir a variável.
echo $result;


echo '<br>';
echo '<br>';


//Variáveis Pré definidas - Super Globais.
//GLOBALS
function cofre($meu_segredo)
{
    $meu_segredo = '7 chaves.';
    $GLOBALS['meu_segredo'] = $meu_segredo;
}

cofre($nome);
echo $GLOBALS['meu_segredo'];


echo '<br>';
echo '<br>';


function cofre2()
{
    $meu_segredo = '7 chaves.';
    $GLOBALS['meu_segredo'] = $meu_segredo;
}

cofre2();
echo $GLOBALS['meu_segredo'];


echo '<br>';
echo '<br>';


//Variável $_SERVER.
foreach ($_SERVER as $chave => $valor) {
    echo $chave . '<br>' . $valor . '<br><br>';
}


echo '<br>';
echo '<br>';

//no navegador http://127.0.0.1:8000/?produto=camiseta&preco=10 - vai imprimir camiseta.
/* $nome = addslashes($_GET['produto']);   //Com addslashes fica mais seguro.
    $preco = $_GET['preco'];

echo $nome, $preco; */


echo '<br>';
echo '<br>';


//Manipulação de arquivos

//Abrindo arquivos, o arquivo deve estar na pasta public e usar o caminho public_path('nomedoarquivo')
$arquivo = fopen(public_path('texto.txt'), 'r');

while (($linha = fgets($arquivo, 4096)) !== false) {
    echo $linha . '<br>';
}



echo '<br>';
echo '<br>';


//Lendo arquivos => file
$arquivo = file(public_path('texto.txt'));   //A variável $arquivo virou um array com os dados do arquivo de texto.

/* var_dump($arquivo); */

for ($linha = 0; $linha < count($arquivo); $linha++) {
    echo $arquivo[$linha] . '<br>';
}


echo '<br>';
echo '<br>';


foreach ($arquivo as $linha) {
    echo $linha . '<br>';
}


//Transformando arquivo para string - file_get_content
$texto = file_get_contents(public_path('texto.txt'));

echo $texto;



echo '<br>';
echo '<br>';


//Escrevendo em arquivos
$poema = fopen(public_path('texto2.txt'), 'r');
//Gravando
$gravar = fopen(public_path('primeira_palavra.txt'), 'w');

while (($linha = fgets($poema, 4096)) !== false) {

    //echo $linha. '<br>';

    $palavra = explode(' ', $linha);

    $primeira_palavra = $palavra[0];

    fwrite($gravar, $primeira_palavra);
}

fclose($gravar);
fclose($poema);


echo '<br>';
echo '<br>';


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
</head>

<body>
    <?php $arquivo = fopen(public_path('texto3.csv'), 'r'); ?>
    <div class="container">
        <table border="1" class="table table-striped table-hover table-sm">

            <?php while ($linha = fgets($arquivo)) : ?>

                <tr>
                    <?php $celulas = explode(',', $linha); ?>

                    <?php foreach ($celulas as $celula) : ?>
                        <td><?= $celula ?></td>
                    <?php endforeach; ?>

                </tr>
            <?php endwhile; ?>

        </table>
    </div>
</body>

</html>

<?php


echo '<br>';
echo '<br>';

//Programação orientada a objetos.
//Classes.
include 'Pessoa.php';

$pessoa1 = new Pessoa();
$pessoa1->nome = 'José.';
$pessoa1->saudar();

$pessoa2 = new Pessoa();
$pessoa2->nome = 'Maria.';
$pessoa2->saudar();

$pessoa3 = new Pessoa();
$pessoa3->nome = 'Carlos.';
$pessoa3->saudar();


//Imprimindo classe private e protected
require_once 'Pessoa2.php';

$pessoa1 = new Pessoa2();
$pessoa1->nome = 'José.';


echo $pessoa1->a;

echo '<br>';
$pessoa1->ler_b();
echo '<br>';
$pessoa1->ler_c();


echo '<br>';
echo '<br>';

//Alterando classe private ou protected
require_once 'Pessoa3.php';


$pessoa1 = new Pessoa3();

$pessoa1->setNome('Marcos');
$pessoa1->setCpf('947.458.897-86');

echo 'Nome: ' . $pessoa1->getNome() . '<br> CPF: ' . $pessoa1->getCpf();


echo '<br>';
echo '<br>';

//Usando construct
require_once 'Pessoa4.php';

$pessoa1 = new Pessoa4('Marcílio', '555.444.666-00');
echo 'Nome: ' . $pessoa1->getNome() . '<br> CPF: ' . $pessoa1->getCpf();

echo '<br>';

$pessoa2 = new Pessoa4('Benedita', '558.466.879-89');
echo 'Nome: ' . $pessoa2->getNome() . '<br> CPF: ' . $pessoa2->getCpf();


echo '<br>';
echo '<br>';

//Usando resolução de escobo.
require_once 'Pessoa5.php';

$pessoa1 = new Pessoa5('Rui', '777.444.666-07');
echo 'Nome: ' . $pessoa1->getNome() . '<br> CPF: ' . $pessoa1->getCpf();

echo '<br>';

$pessoa2 = new Pessoa5('Benila', '999.466.879-86');
echo 'Nome: ' . $pessoa2->getNome() . '<br> CPF: ' . $pessoa2->getCpf();



echo '<br>';
echo '<br>';


//Programação Funcional com PHP.
//Declaração de funções.

function soma_1($a, $b)
{
    //Procedimentos.
    echo $a + $b;
}

soma_1(2, 2);


echo '<br>';
echo '<br>';

function soma_2($a, $b)
{
    //Funções.
    return $a + $b;
}

echo soma_2(2, 6);


echo '<br>';
echo '<br>';


//Funções anônimas.
function ($a, $b) {
    return $a + $b;
};


//Fist-class function => caracteristicas de uma linguagem de programação de aplicar funções a variáveis.
//Function expression

$soma = function ($a, $b) {
    return $a + $b;
};

echo $soma(30, 30);


echo '<br>';
echo '<br>';


//IIFE (Immediately invoked function expression).

(function ($a, $b) {
    echo $a + $b;
})(44, 56);

echo '<br>';

(function ($a, $b) {echo $a + $b;})(44, 56); //Mesma função que a anterior, mas em uma linha.


echo '<br>';
echo '<br>';


$soma = fn($a, $b) => $a + $b;

echo $soma(54, 34);
