<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <h1>Trabalhando com numeros aleatórios</h1>
        </header>
        <section>
            <p>Clique em gerar para receber um numero aleatório.</p>
            <form action="num_ale" method="get">                
                <input type="submit" value="Gerar">
            </form>            
        </section>
    </body>
</html>
