<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <h1>Conversor de moedas</h1>
        </header>
        <section>
            <h2>Quanto você tem na carteira?</h2>
            <form action="conversor" method="get">
                <label for="valor" >Digite o valor:</label>
                <input type="number" name="valor" value="" step="0.01">
                <input type="submit" name="" value="Converter">
            </form>
            
        </section>
    </body>
</html>