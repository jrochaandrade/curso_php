<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <h1>Escolha um numero.</h1>
        </header>
        <section>
            <form action="escolha" method="get">
                <label for="numero">Digite um numero:</label>
                <input type="number" name="numero" value="">
                <input type="submit" name="" value="Enviar">                
            </form>
        </section>
    </body>
</html>