<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>PHP Curso em video</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <h1>PHP Curso em Video</h1>            
        </header>
        <section>
            <h2>Apresente-se para nós!</h2>
            <form action="cad" method="get">
                <label for="nome">Nome</label>
                <input type="text" name="nome" value="" id="idnome">
                <label for="sobrenome">Sobrenome</label>
                <input type="text" name="sobrenome" value="" id="idsobrenome">
                <input type="submit" value="Enviar">
            </form>
        </section>
    </body>
</html>