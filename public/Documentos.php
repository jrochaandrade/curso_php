<?php 


class Documentos{

    private $mascara = '000.000.000-00';

    protected $cpf = '';


    public function getCpf(){
        return $this->cpf;
    }

    public function setCpf($cpf){        

        if(strlen($cpf) == 14){
            $this->cpf = $cpf;
        }
        else{
            echo 'Aviso - Favor digitar CPF:' . $this->mascara. '<br>';
        }
    }

}