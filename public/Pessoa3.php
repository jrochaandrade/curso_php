<?php 

require_once 'Documentos.php';

class Pessoa3 extends Documentos{

    private $nome = '';

    //Ler nome.
    public function getNome(){
        return $this->nome;
    }

    //Alterar nome.
    public function setNome($novo_nome){
        $this->nome = $novo_nome;
    }

}