<?php 


class Documentos2{

    private $mascara = '000.000.000-00';

    protected $cpf = '';


    public function getCpf(){
        return $this->cpf;
    }

    protected function validaCpf($cpf){
        if(strlen($cpf) == 14){
            return $cpf;
        }
        else{
            return 'Aviso - Favor digitar CPF:' . $this->mascara. '<br>';
        }
    }

    public function setCpf($cpf){

       $this->cpf = Documentos2::validaCpf($cpf);
    }

}
